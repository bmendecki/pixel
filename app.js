var
    express = require('express'),
    cookieParser = require('cookie-parser'),
    fs = require('fs'),
    path = require('path'),
    app = express();

app.use(cookieParser());

var user = {
    "user4" : {
        "name" : "mohit",
        "password" : "password4",
        "profession" : "teacher",
        "id": 4
    }
}
app.get('/listusers',function(req,res){
    fs.readFile(__dirname + '/users.json','utf-8',function (err,data) {
        res.end(data);
    })
});

app.post('/addUser', function (req, res) {
    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
        data = JSON.parse( data );
        data["user4"] = user["user4"];
        console.log( data );
        res.end( JSON.stringify(data));
    });
});

app.get('/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
        var users = JSON.parse( data );
        var user = users["user" + req.params.id];
        res.end( JSON.stringify(user));
    });
})

app.delete('/deleteUser/:id',function(req,res){
    fs.readFile(__dirname + '/' + 'users.json','utf-8',function (err,data) {
        data = JSON.parse(data);
        delete data["user" + req.params.id];
        res.end( JSON.stringify(data));
    })
})


app.get('/pixel', function (req,res) {

    var track_id = req.query.track_id,
        date = new Date(),
        ip = req.connection.remoteAddress,
        log = date + ' : ' + track_id + ' : ' + ip + '\n';
        res.cookie('track_id', track_id, { expires: new Date(Date.now() + 7889231498) }); //3 miesiące

    fs.appendFile('log/log.txt', log, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log('saved log');
    });

    res.render('index.ejs');
});
app.use(express.static(path.join(__dirname,'public')));

console.log('listening on 3000');
app.listen(3000);

//localhost/pixel?track_id=X
